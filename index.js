const botaoEnviar = document.getElementById('botao')

const formulario = document.getElementById('assinatura');
botaoEnviar.addEventListener( 'click', () => {
        
        const campoEmail = document.getElementById('campo-email');
        
        const conteudoCampoEmail = campoEmail.value;
        if( conteudoCampoEmail.indexOf('@') >= 0 ) {
            // E-mail ok.
            formulario.submit();
        } else {
            
            alert('Confira o seu e-mail.');
        }
    } 
);